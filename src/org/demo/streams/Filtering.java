package org.demo.streams;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import org.demo.model.Dish;

public class Filtering {
	public static void main(String[] args) {
		
		List<Dish> menu = DataPopulator.getMenu();
		
		//filtering with Predicate
		List<Dish> vegitarian =  menu.stream()
									.filter(Dish :: isVegetarian) //predicate to filter the menu
									.collect(Collectors.toList());
		vegitarian.forEach( a-> System.out.println(a));
		
		
		List<Integer> numbers = Arrays.asList(1, 2, 1, 3, 3, 2, 4);
		//filtering distinct elements
		List<Integer> distinctNumbers = numbers.stream()
											.filter( i -> i % 2 == 0)	
											.distinct()
											.collect(Collectors.toList());
		System.out.println(distinctNumbers);		
		
		//truncating stream using limit
		List<Dish> threeVegetarian = menu.stream().filter(Dish :: isVegetarian).limit(3).collect(Collectors.toList());
		System.out.println(threeVegetarian);
		
		//skipping elements
		List<Dish> skipTwoVegetarian = menu.stream().filter(Dish :: isVegetarian).skip(2).collect(Collectors.toList());
		System.out.println(skipTwoVegetarian);
		
		
		
	}
}
