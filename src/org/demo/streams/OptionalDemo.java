package org.demo.streams;

import java.util.List;
import java.util.Optional;

import org.demo.model.Dish;

public class OptionalDemo {

	public static void main(String[] args) {
		List<Dish> menu = DataPopulator.getMenu();

		// Optional is introduced to handle NullPointerException
		Optional<Dish> dish = menu.stream().filter(Dish::isVegetarian).findAny();
		// checking if element is present before accessing
		if (dish.isPresent()) {
			System.out.println(dish.get());// retrieving the element
		} else {
			System.out.println("dish not found");
		}

		// same thing can also be done as passing a consumer to ifPresent method
		menu.stream().filter(Dish::isVegetarian).findAny().ifPresent(d -> System.out.println(d));
		
		//default value for optional
		dish = dish.empty();//clearing the optional
		Dish tempDish = dish.orElse(menu.get(0));
		System.out.println(tempDish);

	}
}
