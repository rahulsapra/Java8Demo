package org.demo.streams;

import java.util.List;
import java.util.OptionalInt;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import org.demo.model.Dish;

public class IntStreamDemo {
	public static void main(String[] args) {
		
		List<Dish> menu = DataPopulator.getMenu();
		
		//getting stream of primitives. We have LongStream and DoubleStream as well
		IntStream intStream = menu.stream().mapToInt(Dish :: getCalories);
		intStream.forEach( t -> System.out.print(t + " "));
		
		intStream = menu.stream().mapToInt(Dish :: getCalories);
		
		//converting to stream of Integer objects
		Stream<Integer> integerStream = intStream.boxed();
		
		OptionalInt optionalInt = menu.stream().mapToInt(Dish :: getCalories).max();
		int max = optionalInt.orElse(0);
		System.out.println("\nmax: " + max);
		
		
		//ranges
		//closedrange is inclusive
		IntStream closedRangeStream = IntStream.rangeClosed(0, 50).filter( i -> i%2 == 0);
		closedRangeStream.forEach(i -> System.out.print(i + " "));
		
		System.out.println();
		
		//range is exclusive
		IntStream rangeStream = IntStream.range(0, 50).filter( i -> i%2 == 0);
		rangeStream.forEach(i -> System.out.print(i + " "));
		
	}
}
