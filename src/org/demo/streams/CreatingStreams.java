package org.demo.streams;

import java.util.Arrays;
import java.util.stream.Stream;

public class CreatingStreams {
	
	public static void main(String[] args) {
		//creating streams from values
		Stream<String> stream = Stream.of("java", "hello", "world");
		stream.map(String :: toUpperCase).forEach(System.out :: println);
		
		//creating streams from arrays
		int[] array = {10, 20, 30, 40};
		int sum  = Arrays.stream(array).sum();
		System.out.println("sum: " + sum);
		
		//stream from functions. it produces an infinte stream which is also called unbounded stream
		Stream.iterate(0, n -> n + 2).limit(10).forEach(System.out :: print);
		
		System.out.println();
		
		//generate functions applies a function on the value
		Stream.generate(Math :: random).limit(5).forEach( System.out :: print);
	}
}
