package org.demo.streams;

import java.util.List;
import java.util.stream.Stream;

import org.demo.model.Dish;

public class ArrayToStream {
	public static void main(String[] args) {
		List<Dish> menu = DataPopulator.getMenu();
		
		Dish[] dishes = (Dish[]) menu.toArray();
		
		//getting stream from an array
		Stream<Dish> stream = Stream.of(dishes);
		stream.forEach(d -> System.out.println(d));
	}
}
