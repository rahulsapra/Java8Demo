package org.demo.streams;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import org.demo.model.Dish;

public class Mapping {
	public static void main(String[] args) {

		List<Dish> menu = DataPopulator.getMenu();

		// collecting only names of dishes
		List<String> dishNames = menu.stream().map(Dish::getName).collect(Collectors.toList());
		System.out.println(dishNames);

		// getting list of string length in a list
		List<String> words = Arrays.asList("Java8", "Lambdas", "In", "Action");
		List<Integer> lstWordLength = words.stream().map(String::length).collect(Collectors.toList());
		System.out.println(lstWordLength);

		// finding the length of the name of each dish
		List<Integer> dishNameLengths = menu.stream().map(Dish::getName).map(String::length)
				.collect(Collectors.toList());
		System.out.println("dishNameLengths:  " + dishNameLengths);

		// -----------FLATMAP DEMO

		List<Integer> numbers1 = Arrays.asList(1, 2, 3);
		List<Integer> numbers2 = Arrays.asList(3, 4);

		// getting all the pairs from the two list
		List<int[]> pairs = numbers1.stream().flatMap(i -> numbers2.stream().map(j -> new int[] { i, j }))
				.collect(Collectors.toList());
		pairs.forEach(a -> System.out.println(a[0] + "," + a[1]));

		System.out.println("pairs divisible by 3");

		List<int[]> pairDivisibleByThreee = numbers1.stream()
				.flatMap(i -> numbers2.stream().filter(j -> (i + j) % 3 == 0).map(j -> new int[] { i, j }))
				.collect(Collectors.toList());
		pairDivisibleByThreee.forEach(a -> System.out.println(a[0] + "," + a[1]));

	}
}
