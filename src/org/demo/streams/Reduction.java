package org.demo.streams;

import java.util.Arrays;
import java.util.List;

public class Reduction {

	public static void main(String[] args) {

		List<Integer> numbers = Arrays.asList(1, 2, 3);

		// 0 is the initial value
		int sum = numbers.stream().reduce(0, (a, b) -> a + b);
		System.out.println("sum: " + sum);
		// adding using sum static method of Integer
		int sum2 = numbers.stream().reduce(0, Integer::sum);
		System.out.println("sum2: " + sum2);

		int product = numbers.stream().reduce(1, (a, b) -> a * b);
		System.out.println("product: " + product);

		//counting the number of elements
//		int count = numbers.stream().map(d -> 1).reduce(0, (a, b) -> a + b);
		int count = numbers.stream().map(d -> 1).reduce(0, (a, b) -> a + 1); //same as above statement
		System.out.println("count: " + count);

	}

}
