package org.demo.streams;

import java.util.List;

import org.demo.model.Dish;

public class Matching {

	public static void main(String[] args) {
		List<Dish> menu = DataPopulator.getMenu();
		
		//match any element
		if(menu.stream().anyMatch(Dish :: isVegetarian)) {
			System.out.println("Vegetarian dishes found");
		}
		
		//all match
		if(menu.stream().allMatch(Dish :: isVegetarian)) {
			System.out.println("All dishes are vegetarian");
		}
		else {
			System.out.println("Not all dishes are vegetarian");
		}
		
		//none match
		if(menu.stream().noneMatch(Dish :: isVegetarian)) {
			System.out.println("No dishes are vegetarian");
		}
		else {
			System.out.println("Atleast one dish is vegetarian");
		}
		
		
	}
	
	
}
