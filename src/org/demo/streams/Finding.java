package org.demo.streams;

import java.util.Arrays;
import java.util.List;

public class Finding {

	public static void main(String[] args) {
		// finding the first integer whose square is divisible by 3
		List<Integer> numbers = Arrays.asList(1, 2, 3, 4, 5, 6, 7,8,9,10);
		// findFirst 
		numbers.stream().map(x -> x * x).filter(x -> x % 3 == 0).findFirst().ifPresent(x -> System.out.println(x));

		//findAny. findAny is more crucial in parallel streams
		numbers.parallelStream().map(x -> x * x).filter(x -> x % 3 == 0).findAny().ifPresent(x -> System.out.println(x));
		
	}
}
