package org.demo.streams;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import org.demo.model.Trader;
import org.demo.model.Transaction;

public class Exercise {

	public static void main(String[] args) {

		// populating the data
		Trader raoul = new Trader("Raoul", "Cambridge");
		Trader mario = new Trader("Mario", "Milan");
		Trader alan = new Trader("Alan", "Cambridge");
		Trader brian = new Trader("Brian", "Cambridge");
		List<Transaction> transactions = Arrays.asList(new Transaction(brian, 2011, 300),
				new Transaction(raoul, 2012, 1000), new Transaction(raoul, 2011, 400),
				new Transaction(mario, 2012, 710), new Transaction(mario, 2012, 700), new Transaction(alan, 2012, 950));

		// Find all transactions in the year 2011 and sort them by value (small
		// to high).
		// predicate to compare on
		Predicate<Transaction> predicateTransactionYear = new Predicate<Transaction>() {

			@Override
			public boolean test(Transaction t) {
				// TODO Auto-generated method stub
				return t.getYear() == 2011;
			}
		};
		// // sorted in descending order
		// transactions.stream().filter(predicateTransactionYear)
		// .sorted((t1, t2) -> (t1.getValue() <= t2.getValue() ? 1 :
		// -1)).collect(Collectors.toList())
		// .forEach(t -> System.out.println(t));

		System.out.println("Find all transactions in the year 2011 and sort them by value (small to high).");
		
		// sorted in ascending order
		transactions.stream().filter(predicateTransactionYear)
				.sorted((t1, t2) -> (t1.getValue() <= t2.getValue() ? -1 : 1)).collect(Collectors.toList())
				.forEach(t -> System.out.println(t));

		// What are all the unique cities where the traders work?
		System.out.println("What are all the unique cities where the traders work?");
		transactions.stream().map(t -> t.getTrader().getCity()).distinct().forEach(c -> System.out.println(c));

		// Find all traders from Cambridge and sort them by name.
		System.out.println("Find all traders from Cambridge and sort them by name.");
//		transactions.stream().map(t -> t.getTrader()).filter(t -> t.getCity().equalsIgnoreCase("Cambridge"))
//				.sorted((t1, t2) -> t1.getName().compareTo(t2.getName())).distinct()
//				.forEach(t -> System.out.println(t));
		
		//other way to do this
		transactions.stream().map(t -> t.getTrader()).filter(t -> t.getCity().equalsIgnoreCase("Cambridge"))
		.sorted(Comparator.comparing(Trader :: getName)).distinct()
		.forEach(t -> System.out.println(t));

		// Return a string of all traders� names sorted alphabetically.
		System.out.println("Return a string of all traders� names sorted alphabetically.");
//		transactions.stream().map(t -> t.getTrader().getName()).distinct().sorted().forEach(t -> System.out.println(t));
		String str = transactions.stream().map(t -> t.getTrader().getName()).distinct().sorted().reduce("", (a,b) -> a + " " + b);
		System.out.println(str);
		
		
		// Are any traders based in Milan?
		System.out.println("Are any traders based in Milan?");
//		Optional<String> trad = transactions.stream().map(t -> t.getTrader().getCity()).filter((String t) -> {return t.equals("Milan");} ).findAny();
//		Optional<String> trad = transactions.stream().map(t -> t.getTrader().getCity()).filter(t -> t.equals("Milan") ).findAny();
//		System.out.println(trad.isPresent());
		boolean isPresent = transactions.stream().anyMatch(transaction -> transaction.getTrader().getCity().equals("Milan"));
		System.out.println(isPresent);
		
		
		//Print all transactions� values from the traders living in Cambridge
		System.out.println("Print all transactions� values from the traders living in Cambridge");
		transactions.stream().filter( t -> t.getTrader().getCity().equals("Cambridge")).map(t -> t.getValue()).forEach(t -> System.out.println(t));
		
		//What�s the highest value of all the transactions?
		System.out.println("What�s the highest value of all the transactions?");
		Optional<Integer> highestValue = transactions.stream().map(t -> t.getValue()).reduce(Integer :: max);
		System.out.println(highestValue.get());
		
		
		//Find the transaction with the smallest value.
		System.out.println("Find the transaction with the smallest value.");
//		transactions.stream().sorted( (t1, t2) -> t1.getValue() <= t2.getValue() ? -1 : 1).findFirst().ifPresent(t -> System.out.println(t));
		Optional<Transaction> smallestTransaction =
				transactions.stream()
				.min(Comparator.comparing(Transaction::getValue));
		System.out.println(smallestTransaction.get());
		
	}

}
