package org.demo.functions;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Predicate;

class A {
	int a;
	int b;

	public A(int a, int b) {
		super();
		this.a = a;
		this.b = b;
	}

	public int getA() {
		return a;
	}

	public void setA(int a) {
		this.a = a;
	}

	public int getB() {
		return b;
	}

	public void setB(int b) {
		this.b = b;
	}

	public String toString() {
		return a + " " + b;
	}

}

class FilterByAPredicate implements Predicate<A> {

	/**
	 * method to return true if a> 10
	 */
	@Override
	public boolean test(A t) {
		if (t.getA() > 10) {
			return true;
		}
		return false;
	}

}

/**
 * java.util.function package provides implementation for Predicate. So we can
 * use it directly instead of defining a predicate on our own
 * 
 *
 */
public class PredicateDemo {

	public static List<A> filterA(List<A> lstA, Predicate<A> p) {
		List<A> result = new ArrayList<>();
		for (A a : lstA) {
			if (p.test(a)) {
				result.add(a);
			}
		}
		return result;

	}

	public static void main(String[] args) {
		List<A> lst = new ArrayList<>();
		lst.add(new A(10, 20));
		lst.add(new A(1, 2));
		lst.add(new A(11, 12));
		lst.add(new A(19, 2));
		lst.add(new A(0, 20));

		FilterByAPredicate predicate = new FilterByAPredicate();
		Predicate<A> negatePredicate = predicate.negate();
		Predicate<A> andPredicate = predicate.and(negatePredicate);
		Predicate<A> orPredicate = predicate.or(negatePredicate);

		System.out.println("PREDICATE");
		List<A> filteredA = filterA(lst, predicate);
		System.out.println(filteredA);

		// negating a predicate
		System.out.println("NEGATE PREDICATE");
		List<A> negateFilteredA = filterA(lst, negatePredicate);
		System.out.println(negateFilteredA);

		// and predicate
		System.out.println("AND PREDICATE");
		List<A> andFilteredA = filterA(lst, andPredicate);
		System.out.println(andFilteredA);

		// and predicate
		System.out.println("OR PREDICATE");
		List<A> orFilteredA = filterA(lst, orPredicate);
		System.out.println(orFilteredA);

	}

}
