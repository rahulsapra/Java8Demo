package org.demo.functions;

import java.util.List;
import java.util.function.Supplier;

import org.demo.model.Dish;
import org.demo.streams.DataPopulator;

/**
 * supplier is a functional interface that takes no argument and return an
 * object of a type Supplier has a get method that takes no argument and returns
 * an object
 * 
 * @author rahul
 *
 */
public class SupplierDemo {

	public static void main(String[] args) {
		// create a supplier to
		Supplier<String> supplier = () -> "hello world";
		System.out.println(supplier.get());

		List<Dish> menu = DataPopulator.getMenu();

		Supplier<Dish> dishSupplier = () -> menu.get(0);
		System.out.println(dishSupplier.get());

	}
}
