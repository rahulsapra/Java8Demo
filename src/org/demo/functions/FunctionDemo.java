package org.demo.functions;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.function.Function;

/**
 * Function interface is present in the java.util.function package that has an
 * abstract method R apply(T t). It can be used in cases where we need to map
 * one object to another
 *
 */
public class FunctionDemo {

	public static <T, R> List<R> process(List<T> lst, Function<T, R> f) {
		List<R> result = new ArrayList<>();
		for (T t : lst) {
			result.add(f.apply(t));
		}
		return result;
	}

	public static void main(String[] args) {
		List<Integer> lengths = process(Arrays.asList("hello", "world", "google"), t -> t.length());
		System.out.println(lengths);
	}

}
