package org.demo.functions;

import java.util.Arrays;
import java.util.List;
import java.util.function.Consumer;

//Consumer interface is present in function package. It provides a method void accept(T t)
//it can be used to carry an operation without returning anything.


public class ConsumerDemo {

	
	public static <T> void processArray(List<T> lst, Consumer<T> consumer) {
//		run the accept method on each element of the list
//		can be useful when we need to do a number of operations on the elemnet of the array
		for(T t: lst) {
			consumer.accept(t);
		}
	}
	
	public static void main(String[] args) {
		processArray(Arrays.asList(1,2,3,4,5,6,7,8,9,10), 
				i -> System.out.println(i) //passing the consumer using lambdas				
				);
		
	}
	
}
