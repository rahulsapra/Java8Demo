package org.demo.model;

public class Person {

	int age;
	String name;

	public Person(int age, String name) {
		this.age = age;
		this.name = name;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public static int compareAges(Person p1, Person p2) {
		Integer age = p1.getAge();
		return age.compareTo(p2.getAge());
	}

	public String toString() {
		return name + " -> " + age;
	}

}
