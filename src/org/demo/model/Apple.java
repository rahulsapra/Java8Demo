package org.demo.model;

public class Apple {
	int weight;

	public Apple(int weight) {
		super();
		this.weight = weight;
	}

	public int getWeight() {
		return weight;
	}

	public void setWeight(int weight) {
		this.weight = weight;
	}

	public String toString() {
		return String.valueOf(weight);
	}

}