package org.demo.collections;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.demo.model.Dish;
import org.demo.streams.DataPopulator;

/**
 * partitioning is used to get two categories of data. it takes a predicate instead of a classification function. The 
 * resultant map has two values: true and false 
 * 
 * @author rahul
 *
 */
public class Partitioning {
	public static void main(String[] args) {
		
		List<Dish> menu = DataPopulator.getMenu();
		
		//get the vegetarian and non-vegetarian dishes
		Map<Boolean, List<Dish>> partitionedMap = menu.stream().collect(Collectors.partitioningBy(Dish :: isVegetarian));
		System.out.println("veg dishes: " + partitionedMap.get(true));
		System.out.println("non-veg dishes: " + partitionedMap.get(false));
	}
}
