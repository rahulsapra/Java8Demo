package org.demo.collections;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import org.demo.model.Trader;
import org.demo.model.Transaction;


public class Introduction {
	public static void main(String[] args) {
		Trader raoul = new Trader("Raoul", "Cambridge");
		Trader mario = new Trader("Mario", "Milan");
		Trader alan = new Trader("Alan", "Cambridge");
		Trader brian = new Trader("Brian", "Cambridge");
		List<Transaction> transactions = Arrays.asList(new Transaction(brian, 2011, 300),
				new Transaction(raoul, 2012, 1000), new Transaction(raoul, 2011, 400),
				new Transaction(mario, 2012, 710), new Transaction(mario, 2012, 700), new Transaction(alan, 2012, 950));
		
		Map<Trader, List<Transaction>> mapTransactionsByTrader = transactions.stream().collect(Collectors.groupingBy(Transaction :: getTrader));
		mapTransactionsByTrader.forEach((tr, trns) ->  System.out.println(tr + " " + trns));
//		System.out.println(mapTransactionsByTrader);
		
		//counting 
		long num = transactions.stream().count();
		System.out.println("num: " + num);
		
		//getting transaction with highest value
		Comparator<Transaction> transComparator = Comparator.comparingInt(Transaction :: getValue);
		
		Optional<Transaction> maxTrans = transactions.stream().collect(Collectors.maxBy(transComparator));
		System.out.println(maxTrans.get());
		
	}
}
