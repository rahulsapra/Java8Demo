package org.demo.collections;

import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collector;
import java.util.stream.Collectors;

import javax.swing.plaf.basic.BasicInternalFrameTitlePane.MaximizeAction;

import org.demo.model.Dish;
import org.demo.streams.DataPopulator;

public class Grouping {
	public static void main(String[] args) {
		List<Dish> menu = DataPopulator.getMenu();

		// grouping dishes by type
		// the method passed in groupingBy is called classification
		Map<Dish.Type, List<Dish>> dishesByType = menu.stream().collect(Collectors.groupingBy(Dish::getType));
		dishesByType.forEach((t, list) -> System.out.println(t + " " + list));

		// using custom classification function
		// classifying dishes as normal or diet based on their calorific value
		Map<String, List<Dish>> dishesByCalories = menu.stream().collect(Collectors.groupingBy(dish -> {

			if (dish.getCalories() < 400) {
				return "diet";
			} else
				return "normal";
		}));

		dishesByCalories.forEach((s, list) -> System.out.println(s + " " + list));

		System.out.println("multilevel grouping");
		// multilevel grouping
		// classifying dishes as type and then as normal and diet based on their
		// calorific value
		Map<Dish.Type, Map<String, List<Dish>>> multipleGrouping = menu.stream()
				.collect(Collectors.groupingBy(Dish::getType, Collectors.groupingBy(dish -> {

					if (dish.getCalories() < 400) {
						return "diet";
					} else
						return "normal";
				})));
		System.out.println(multipleGrouping);

		// mapping the result into another type. this is used to adapt the result of a
		// collector into another type. collectingAndThen is used to achieve this
		Map<Dish.Type, Dish> mapDishes = menu.stream().collect(Collectors.groupingBy(Dish::getType, Collectors
				.collectingAndThen(Collectors.maxBy(Comparator.comparingInt(Dish::getCalories)), Optional::get)));
		System.out.println("mapDishes: " + mapDishes);

		// getting the dish with highest calorie in each type
		// if the Optional.value() does not has any value, then the key is not added
		// into the map
		// the keys are added lazily when the first value is found
		Map<Dish.Type, Optional<Dish>> mapMaxValueByType = menu.stream().collect(
				Collectors.groupingBy(Dish::getType, Collectors.maxBy(Comparator.comparingInt(Dish::getCalories))));
		System.out.println("printing dish with highest calorific value for each type");
		System.out.println(mapMaxValueByType);
		
		//mapping collectorused in conjunction with grouping by
		Map<Dish.Type, Set<String>> caloricLevelsByType =
				menu.stream().collect(
				Collectors.groupingBy(Dish::getType, Collectors.mapping(
				dish -> { if (dish.getCalories() <= 400) return "DIET";
				else if (dish.getCalories() <= 700) return "NORMAL";
				else return "FAT"; },
				Collectors.toSet() )));
		System.out.println("get calorific levels available for each type of dish");
		System.out.println(caloricLevelsByType);

	}

}
