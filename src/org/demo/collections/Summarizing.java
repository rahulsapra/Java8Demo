package org.demo.collections;

import java.util.List;

import org.demo.model.Dish;
import org.demo.streams.DataPopulator;

import static java.util.stream.Collectors.*;

import java.util.IntSummaryStatistics;

public class Summarizing {
	public static void main(String[] args) {
		List<Dish> menu = DataPopulator.getMenu();
		
		//all these oprations are available for long and double as well
		
		//summing
//		int sum = menu.stream().collect(summingInt(Dish :: getCalories));
//		double sum = menu.stream().collect(summingDouble(Dish :: getCalories));
		long sum = menu.stream().collect(summingLong(Dish :: getCalories));
		System.out.println("total calories: " + sum);
		
		//average 
		double avgCalories = menu.stream().collect(averagingInt(Dish :: getCalories));
		System.out.println("average calories: " + avgCalories);
		
		
		//getting all stats in one opration
		IntSummaryStatistics menuStats = menu.stream().collect(summarizingInt(Dish :: getCalories));
		System.out.println("stats summary : " + menuStats);
		
	}
}	
