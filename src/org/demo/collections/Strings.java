package org.demo.collections;

import java.util.List;

import org.demo.model.Dish;
import org.demo.streams.DataPopulator;

import static java.util.stream.Collectors.*;

public class Strings {
	public static void main(String[] args) {
		// collector returned by the joining method returns the sting formed by
		// concatinating strings obtained by calling toString on all objects inthe
		// collection

		List<Dish> menu = DataPopulator.getMenu();
		
		
		//concatinating the dishes' names
		String shortMenu = menu.stream().map(Dish :: getName).collect(joining());
		System.out.println("shortMenu: " + shortMenu);
		
		//concating the dishes' names separated by a delimiter
		String longMenu = menu.stream().map(Dish:: getName).collect(joining(", "));
		System.out.println("longMenu: " + longMenu);
		
	}
}
