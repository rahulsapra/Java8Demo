package org.demo.collections;

import java.util.List;
import java.util.stream.Collectors;

import org.demo.model.Dish;
import org.demo.streams.DataPopulator;

public class Reducing {
	public static void main(String[] args) {
		List<Dish> menu = DataPopulator.getMenu();
		
		int totalCalories = menu.stream().map(Dish :: getCalories).reduce(Integer :: sum).get();
		System.out.println("total calories:" + totalCalories);
		
		//same thing using reducing
		int totalCal = menu.stream().collect(Collectors.reducing(0, Dish :: getCalories, (i,j) -> i+j));
		System.out.println("totalCal: " + totalCal);
		
		int sum = menu.stream().collect(Collectors.reducing(0, Dish :: getCalories, Integer :: sum));
		System.out.println("sum : " + sum);
		
		//one more alternative
		int sum4 = menu.stream().mapToInt(Dish :: getCalories).sum();
		System.out.println("sum4: " + sum4);
		
	}
}
