package org.demo.methodreference;

import java.util.function.BiFunction;
import java.util.function.Function;

import org.demo.model.Apple;
import org.demo.model.Person;

public class ConstructorReference {

	public static void main(String[] args) {
		//passing constructor as argument to function
		Function<Integer, Apple> function = Apple :: new;
		Apple a = function.apply(10);
		System.out.println(a);
		
		
		//using BIFunction if the constructor takes two arguments
		BiFunction<Integer, String, Person> biFunction = Person :: new;
		Person p = biFunction.apply(10 , "john");
		System.out.println(p);
		
	}
}
