package org.demo.methodreference;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.demo.model.Person;

public class InstanceMethodReference {

	public static void main(String[] args) {
		InstanceMethodReference refer = new InstanceMethodReference();
		refer.sortData();
	}
	
	public void sortData() {
		List<Person> persons = new ArrayList<>();

		persons.add(new Person(1, "a"));
		persons.add(new Person(2, "b"));
		persons.add(new Person(3, "c"));
		persons.add(new Person(5, "d"));
		persons.add(new Person(41, "e"));
		
		//instance method reference
		Collections.sort(persons, this :: compareAges);
		System.out.println(persons);
	}
	
	public int compareAges(Person p1, Person p2) {
		Integer age = p1.getAge();
		return age.compareTo(p2.getAge());
	}
}
