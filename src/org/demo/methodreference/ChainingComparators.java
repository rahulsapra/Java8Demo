package org.demo.methodreference;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

import org.demo.model.Person;

public class ChainingComparators {

	public static void main(String[] args) {
		List<Person> persons = new ArrayList<>();

		persons.add(new Person(1, "b"));
		persons.add(new Person(1, "a"));
		persons.add(new Person(1, "c"));
		persons.add(new Person(1, "d"));
		persons.add(new Person(11, "e"));
		
		//sort person by age and then by name if ages equal -- decreasing order
		persons.sort(Comparator.comparing(Person :: getAge).reversed().thenComparing(Person :: getName).reversed());
		System.out.println(persons);
	}
	
}
