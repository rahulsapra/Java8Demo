package org.demo.methodreference;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

import org.demo.model.*;

public class MethodReferenceDemo {

	public static void main(String[] args) {
		List<Apple> inventory = new ArrayList<>();
		inventory.add(new Apple(10));
		inventory.add(new Apple(20));
		inventory.add(new Apple(150));
		inventory.add(new Apple(12));
		inventory.add(new Apple(25));

		// sorting using lambdas
		// inventory.sort(Comparator.comparing(a -> a.getWeight()));

		// Comparator.comparing takes method reference as argument
		inventory.sort(Comparator.comparing(Apple::getWeight));

		System.out.println(inventory);

		System.out.println("sorting in decreasing order");
		inventory.sort(Comparator.comparing(Apple::getWeight).reversed());

		System.out.println(inventory);

	}


}
