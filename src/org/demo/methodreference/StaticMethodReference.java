package org.demo.methodreference;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.demo.model.Person;

public class StaticMethodReference {

	public static void main(String[] args) {
		List<Person> persons = new ArrayList<>();

		persons.add(new Person(1, "a"));
		persons.add(new Person(2, "b"));
		persons.add(new Person(3, "c"));
		persons.add(new Person(5, "d"));
		persons.add(new Person(41, "e"));

		Collections.sort(persons, Person::compareAges);
		persons.forEach(p -> System.out.println(p));

		// same can be done using
		// persons.sort(Person :: compareAges);
		// System.out.println(persons);

	}

}
