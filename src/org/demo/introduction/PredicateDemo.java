package org.demo.introduction;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

import org.omg.CosNaming.IstringHelper;

public class PredicateDemo {

	public interface Predicate<T> {
		boolean test(T t);
	}

	// method to filter apples
	public static List<Apple> filterApples(List<Apple> inventory, Predicate<Apple> p) {
		List<Apple> result = new ArrayList<>();
		for (Apple apple : inventory) {
			if (p.test(apple)) {
				result.add(apple);
			}
		}
		return result;
	}

	public static void main(String[] args) {
		// testing the code
		List<Apple> apples = new ArrayList<>();
		apples.add(new Apple(12, "red"));
		apples.add(new Apple(120, "green"));
		apples.add(new Apple(200, "green"));
		apples.add(new Apple(400, "yellow"));
		apples.add(new Apple(9002, "orange"));

		// print green apples
		System.out.println(filterApples(apples, Apple::isGreenApple));

		// print heavy apples
		System.out.println(filterApples(apples, Apple::isHeavy));

		// achieving the same using lambdas
		System.out.println(filterApples(apples, (Apple a) ->  {return"green".equals(a.getColour());}));
//the above statement can also be written as
//		System.out.println(filterApples(apples, (Apple a) -> "green".equals(a.getColour())));
//		System.out.println(filterApples(apples, (Apple a) ->  "green".equals(a.getColour())));
		
		
		
		// sequenetial stream
		System.out.println(
				apples.stream().filter((Apple a) -> "green".equals(a.getColour())).collect(Collectors.toList()));
		
		//parallel stream
		System.out.println(apples.parallelStream().filter((Apple a) -> "green".equals(a.getColour())).collect(Collectors.toList()));
		
		
		//creating comparator using java
		Comparator<Apple> byWeight = (a1,a2) -> ((Integer)a1.getWeight()).compareTo(a2.getWeight());
		
		apples.sort(byWeight);
		System.out.println(apples);

	}

}

class Apple {
	private int weight;
	private String colour;

	public Apple() {
	}

	public Apple(int weight, String colour) {
		super();
		this.weight = weight;
		this.colour = colour;
	}

	public int getWeight() {
		return weight;
	}

	public void setWeight(int weight) {
		this.weight = weight;
	}

	public String getColour() {
		return colour;
	}

	public void setColour(String colour) {
		this.colour = colour;
	}

	static boolean isGreenApple(Apple apple) {
		return "green".equals(apple.getColour());
	}

	static boolean isHeavy(Apple apple) {
		return apple.getWeight() > 150;
	}

	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return "weight: " + weight + " colour: " + colour;
	}

}