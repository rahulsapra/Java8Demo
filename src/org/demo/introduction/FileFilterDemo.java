package org.demo.introduction;
import java.io.File;
import java.io.FileFilter;

public class FileFilterDemo {

	public static void main(String[] args) {
		// code used to print all files prior to java 8
		File directory  = new File("E:/code");
		File[] myFiles = directory.listFiles(new FileFilter() {
			
			@Override
			public boolean accept(File pathname) {
				// TODO Auto-generated method stub
				return pathname.isFile();
			}
		});
		
		for(File file : myFiles) {
			System.out.println(file.getName());
		}
		
		
		//code used in java 8
		File[] files = directory.listFiles(File::isFile);
		for(File file : files) {
			System.out.println(file.getName());
		}
		
		
	}
	
	
	
	
	
}
